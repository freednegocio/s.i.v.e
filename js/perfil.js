const buttonModificar = $('#buttonModificar')
buttonModificar.click(function () {
    buttonModificar.hide();
    $('#buttonGuardar').show();

    $('#inputNombre').prop('disabled', false);
    $('#inputApellido').prop('disabled', false);
    $('#inputEdad').prop('disabled', false);
    $('#inputEmail').prop('disabled', false);
    $('#inputTelefono').prop('disabled', false);
    $('#inputImage').prop('disabled', false);
    $('#inputnumCasa').prop('disabled', false);
    $('#inputBarrio').prop('disabled', false);
    $('#inputCalle').prop('disabled', false);
    $('#divImage').prop('style', 'display: initial;')
})

